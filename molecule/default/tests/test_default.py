import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('slackalarm')


def test_files(host):
    assert host.file("/opt/slackalarm/SlackAlarm.py").exists
    assert host.file("/opt/slackalarm/SlackAlarm_config.ini").exists
